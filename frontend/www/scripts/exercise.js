let cancelButton;
let okButton;
let deleteButton;
let editButton;
let oldFormData;

class MuscleGroup {
    constructor(type) {
        this.isValidType = false;
        this.validTypes = [
            'Legs',
            'Chest',
            'Back',
            'Arms',
            'Abdomen',
            'Shoulders',
        ];

        this.type = this.validTypes.includes(type) ? type : undefined;
    }

    setMuscleGroupType = (newType) => {
        this.isValidType = false;

        if (this.validTypes.includes(newType)) {
            this.isValidType = true;
            this.type = newType;
        } else {
            alert('Invalid muscle group!');
        }
    };

    getMuscleGroupType = () => this.type;
}

function setButtonVisibilities(showCancel, showOk, showDelete, showEdit) {
    if (showCancel) cancelButton.className.replace(' hide', '');
    else cancelButton.className += ' hide';

    if (showOk) okButton.className.replace(' hide', '');
    else okButton.className += ' hide';

    if (showDelete) deleteButton.className.replace(' hide', '');
    else deleteButton.className += ' hide';

    if (showEdit) editButton.className.replace(' hide', '');
    else editButton.className += ' hide';
}

function showAlert(alertMessage, data) {
    const alert = createAlert(alertMessage, data);
    document.body.prepend(alert);
}

function insertOldFormDataIntoForm(form) {
    if (oldFormData.has('name')) form.name.value = oldFormData.get('name');
    if (oldFormData.has('description')) { form.description.value = oldFormData.get('description'); }
    if (oldFormData.has('duration')) { form.duration.value = oldFormData.get('duration'); }
    if (oldFormData.has('calories')) { form.calories.value = oldFormData.get('calories'); }
    if (oldFormData.has('muscleGroup')) { form.muscleGroup.value = oldFormData.get('muscleGroup'); }
    if (oldFormData.has('unit')) form.unit.value = oldFormData.get('unit');
}

function deleteOldFormData() {
    oldFormData.delete('name');
    oldFormData.delete('description');
    oldFormData.delete('duration');
    oldFormData.delete('calories');
    oldFormData.delete('muscleGroup');
    oldFormData.delete('unit');
}

function handleCancelButtonDuringEdit() {
    setReadOnly(true, '#form-exercise');

    document.querySelector('select').setAttribute('disabled', '');

    setButtonVisibilities(false, false, false, true);

    cancelButton.removeEventListener('click', handleCancelButtonDuringEdit);

    const form = document.querySelector('#form-exercise');
    insertOldFormDataIntoForm(form);
    deleteOldFormData();
}

function handleCancelButtonDuringCreate() {
    window.location.replace('exercises.html');
}

function formatExerciseForm(form) {
    const formData = new FormData(form);
    return {
        name: formData.get('name'),
        description: formData.get('description'),
        duration: formData.get('duration'),
        calories: formData.get('calories'),
        muscleGroup: formData.get('muscleGroup'),
        unit: formData.get('unit'),
    };
}

async function createExercise() {
    document.querySelector('select').removeAttribute('disabled');
    const form = document.querySelector('#form-exercise');
    const body = formatExerciseForm(form);

    const response = await sendRequest('POST', `${HOST}/api/exercises/`, body);

    if (response.ok) {
        window.location.replace('exercises.html');
    } else {
        const data = await response.json();
        showAlert('Could not create new exercise!', data);
    }
}

function handleEditExerciseButtonClick() {
    setReadOnly(false, '#form-exercise');

    document.querySelector('select').removeAttribute('disabled');

    setButtonVisibilities(true, true, true, false);

    cancelButton.addEventListener('click', handleCancelButtonDuringEdit);

    const form = document.querySelector('#form-exercise');
    oldFormData = new FormData(form);
}

async function deleteExercise(id) {
    const response = await sendRequest(
        'DELETE',
        `${HOST}/api/exercises/${id}/`,
    );
    if (!response.ok) {
        const data = await response.json();
        showAlert(`Could not delete exercise ${id}`, data);
    } else {
        window.location.replace('exercises.html');
    }
}

async function retrieveExercise(id) {
    const response = await sendRequest('GET', `${HOST}/api/exercises/${id}/`);

    if (!response.ok) {
        const data = await response.json();
        showAlert('Could not retrieve exercise data!', data);
        return;
    }
    document.querySelector('select').removeAttribute('disabled');
    const exerciseData = await response.json();
    const form = document.querySelector('#form-exercise');
    const formData = new FormData(form);

    for (const key of formData.keys()) {
        let selector;
        key !== 'muscleGroup'
            ? (selector = `input[name="${key}"], textarea[name="${key}"]`)
            : (selector = `select[name=${key}]`);
        const input = form.querySelector(selector);
        const newVal = exerciseData[key];
        input.value = newVal;
    }
    document.querySelector('select').setAttribute('disabled', '');
}

async function updateExercise(id) {
    const form = document.querySelector('#form-exercise');
    const formData = new FormData(form);

    const muscleGroupSelector = document.querySelector('select');
    muscleGroupSelector.removeAttribute('disabled');

    const body = formatExerciseForm(form);
    body.muscleGroup = new MuscleGroup(formData.get('muscleGroup'));

    const response = await sendRequest(
        'PUT',
        `${HOST}/api/exercises/${id}/`,
        body,
    );

    if (!response.ok) {
        const data = await response.json();
        showAlert(`Could not update exercise ${id}`, data);
    } else {
        muscleGroupSelector.setAttribute('disabled', '');
        setReadOnly(true, '#form-exercise');
        setButtonVisibilities(false, false, false, true);

        cancelButton.removeEventListener('click', handleCancelButtonDuringEdit);

        deleteOldFormData();
    }
}

window.addEventListener('DOMContentLoaded', async () => {
    cancelButton = document.querySelector('#btn-cancel-exercise');
    okButton = document.querySelector('#btn-ok-exercise');
    deleteButton = document.querySelector('#btn-delete-exercise');
    editButton = document.querySelector('#btn-edit-exercise');
    oldFormData = null;

    const urlParams = new URLSearchParams(window.location.search);

    // view/edit
    if (urlParams.has('id')) {
        const exerciseId = urlParams.get('id');
        await retrieveExercise(exerciseId);

        editButton.addEventListener('click', handleEditExerciseButtonClick);
        deleteButton.addEventListener(
            'click',
            (async (id) => await deleteExercise(id)).bind(undefined, exerciseId),
        );
        okButton.addEventListener(
            'click',
            (async (id) => await updateExercise(id)).bind(undefined, exerciseId),
        );
    }
    // create
    else {
        setReadOnly(false, '#form-exercise');

        setButtonVisibilities(true, true, false, false);

        okButton.addEventListener('click', async () => await createExercise());
        cancelButton.addEventListener('click', handleCancelButtonDuringCreate);
    }
});
