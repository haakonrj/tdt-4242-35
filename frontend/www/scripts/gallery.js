const IMAGE_FORMATS = ['jpg', 'png', 'gif', 'jpeg'];
const SELECTED_IMAGE_OPACITY = 0.6;

async function retrieveWorkoutImages(id) {
    const response = await sendRequest('GET', `${HOST}/api/workouts/${id}/`);
    const data = await response.json();
    if (response.ok) {
        return data;
    }
    const alert = createAlert('Could not retrieve workout data!', data);
    document.body.prepend(alert);
}

function appendFileCounterAndIsFirstImgToFile(file, i) {
    return {
        ...file,
        fileCounter: i,
        isFirstImg: i === 0,
    };
}

function displayErrorTextIfNoImages(files) {
    const hasNoImages = files.length === 0;
    const noImageText = document.querySelector('#no-images-text');
    if (hasNoImages) {
        noImageText.classList.remove('hide');
    } else {
        noImageText.classList.add('hide');
    }
    return hasNoImages;
}

function addFadeInAnimationToImage(imageElement) {
    imageElement.classList.add('fade-in');
    setTimeout(() => imageElement.classList.remove('fade-in'), 500);
}

function highlightClickedImage(image, otherImageFileElements, selectedOpacity) {
    const currentImageFileElement = document.querySelector('#current');

    currentImageFileElement.src = image.src;

    addFadeInAnimationToImage(currentImageFileElement);

    otherImageFileElements.forEach(
        (imageFileElement) => (imageFileElement.style.opacity = 1),
    );
    image.style.opacity = selectedOpacity;
}

function setWorkoutTitleAndOwner(data) {
    document.getElementById('workout-title').innerHTML = `Workout name: ${data.name}`;
    document.getElementById('workout-owner').innerHTML = `Owner: ${data.owner_username}`;
}

function addHighlightClickEventToImageElement(image, otherImages) {
    image.addEventListener('click', (event) => {
        const { target } = event;
        highlightClickedImage(target, otherImages, SELECTED_IMAGE_OPACITY);
    });
}

function isValidImageFormat(file) {
    const fileName = file.file.split('/').slice(-1)[0];
    const fileFormat = fileName.split('.')[1].toLowerCase();
    return IMAGE_FORMATS.includes(fileFormat);
}

function createImageAnchorElement(file) {
    const a = document.createElement('a');
    a.href = file.file;
    const fileName = file.file.split('/').slice(-1)[0];
    a.text = fileName;
    a.className = 'me-2';
}

function createImageDeleteButtonElement(file) {
    const deleteImgButton = document.createElement('input');
    deleteImgButton.type = 'button';
    deleteImgButton.className = 'btn btn-close';
    deleteImgButton.id = file.url.split('/')[file.url.split('/').length - 2];
    deleteImgButton.addEventListener('click', () => handleDeleteImgClick(
        deleteImgButton.id,
        'DELETE',
        `Could not delete workout ${deleteImgButton.id}!`,
        HOST,
        IMAGE_FORMATS,
    ));

    deleteImgButton.style.left = `${(file.fileCounter % 4) * 191}px`;
    deleteImgButton.style.top = `${Math.floor(file.fileCounter / 4) * 105}px`;

    const filesDeleteDiv = document.getElementById('img-collection-delete');
    filesDeleteDiv.appendChild(deleteImgButton);
}

function createImgElement(file) {
    const img = document.createElement('img');
    img.src = file.file;
    const filesDiv = document.getElementById('img-collection');
    filesDiv.appendChild(img);
}

function addWorkoutDataToDOM(data) {
    setWorkoutTitleAndOwner(data);

    if (displayErrorTextIfNoImages(data.files)) return;

    data.files
        .map(appendFileCounterAndIsFirstImgToFile)
        .forEach(createWorkoutImageDOMElement);

    const otherImageFileElements = document.querySelectorAll('.imgs img');
    otherImageFileElements[0].style.opacity = SELECTED_IMAGE_OPACITY;
    otherImageFileElements.forEach((image) => addHighlightClickEventToImageElement(image, otherImageFileElements));
}

function createWorkoutImageDOMElement(file) {
    if (!isValidImageFormat(file)) return;

    createImageAnchorElement(file);
    createImageDeleteButtonElement(file);
    createImgElement(file);

    if (file.isFirstImg) {
        const currentImageFileElement = document.querySelector('#current');
        currentImageFileElement.src = file.file;
    }
}

async function validateImgFileType(id, hostVariable, acceptedFileTypes) {
    const file = await sendRequest(
        'GET',
        `${hostVariable}/api/workout-files/${id}/`,
    );
    const fileData = await file.json();
    const fileType = fileData.file
        .split('/')
        [fileData.file.split('/').length - 1].split('.')[1];

    return acceptedFileTypes.includes(fileType);
}

async function handleDeleteImgClick(
    id,
    httpKeyword,
    failAlertText,
    hostVariable,
    acceptedFileTypes,
) {
    if (validateImgFileType(id, hostVariable, acceptedFileTypes)) {
        return;
    }

    const response = await sendRequest(
        httpKeyword,
        `${hostVariable}/api/workout-files/${id}/`,
    );

    if (!response.ok) {
        const data = await response.json();
        const alert = createAlert(failAlertText, data);
        document.body.prepend(alert);
    } else {
        location.reload();
    }
}

function handleGoBackToWorkoutClick() {
    const urlParams = new URLSearchParams(window.location.search);
    const id = urlParams.get('id');
    window.location.replace(`workout.html?id=${id}`);
}

window.addEventListener('DOMContentLoaded', async () => {
    const goBackButton = document.querySelector('#btn-back-workout');
    goBackButton.addEventListener('click', handleGoBackToWorkoutClick);

    const urlParams = new URLSearchParams(window.location.search);
    const id = urlParams.get('id');
    const workoutData = await retrieveWorkoutImages(id);
    addWorkoutDataToDOM(workoutData);
});
