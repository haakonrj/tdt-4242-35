const formIds = [
  "input-profile-username",
  "input-profile-email",
  "input-profile-phone",
  "input-profile-country",
  "input-profile-city",
  "input-profile-street",
];

function disabledFormIds(disable) {
  const inputFields = formIds.map((id) => document.getElementById(id));
  if (disable)
    inputFields.forEach((input) => input.setAttribute("disabled", true));
  else inputFields.forEach((input) => input.removeAttribute("disabled"));
}

async function fetchUserDetails(request) {
  const currentUser = await getCurrentUser();

  const response = await sendRequest(
    "GET",
    `${HOST}/api/users/${currentUser.id}/`
  );

  if (!response.ok) return;

  const data = await response.json();
  const {
    username,
    email,
    phone_number,
    country,
    city,
    street_address,
    is_coach,
  } = data;

  const usernameInput = document.getElementById("input-profile-username");
  usernameInput.value = username;

  const emailInput = document.getElementById("input-profile-email");
  emailInput.value = email;

  const phoneInput = document.getElementById("input-profile-phone");
  phoneInput.value = phone_number;

  const countryInput = document.getElementById("input-profile-country");
  countryInput.value = country;

  const cityInput = document.getElementById("input-profile-city");
  cityInput.value = city;

  const streetInput = document.getElementById("input-profile-street");
  streetInput.value = street_address;

  const isCoachCheckbox = document.getElementById("checkbox-profile-coach");
  isCoachCheckbox.checked = is_coach;
}

document.getElementById("btn-edit-profile").addEventListener("click", () => {
  disabledFormIds(false);

  document.getElementById("btn-edit-profile").classList.add("hide");
  document.getElementById("btn-save-profile").classList.remove("hide");
  document.getElementById("coach-checkbox-container").classList.remove("hide");
});

document
  .getElementById("btn-save-profile")
  .addEventListener("click", async () => {
    const username = document.getElementById("input-profile-username").value;
    const email = document.getElementById("input-profile-email").value;
    const phone_number = document.getElementById("input-profile-phone").value;
    const country = document.getElementById("input-profile-country").value;
    const city = document.getElementById("input-profile-city").value;
    const street_address = document.getElementById(
      "input-profile-street"
    ).value;
    const is_coach = document.getElementById("checkbox-profile-coach").checked;

    const body = {
      username,
      email,
      phone_number,
      country,
      city,
      street_address,
      is_coach,
    };

    const currentUser = await getCurrentUser();

    await sendRequest("PATCH", `${HOST}/api/users/${currentUser.id}/`, body);

    disabledFormIds(true);
    document.getElementById("text-profile-updated").classList.remove("hide");
    document.getElementById("btn-edit-profile").classList.remove("hide");
    document.getElementById("btn-save-profile").classList.add("hide");
    document.getElementById("coach-checkbox-container").classList.add("hide");
  });

fetchUserDetails();
