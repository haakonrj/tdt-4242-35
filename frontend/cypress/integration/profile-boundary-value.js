import { v4 as uuidv4 } from "uuid";

describe("Test register page", () => {
  beforeEach(() => {
    cy.visit("http://localhost:8000");
  });

  it("Boundary test correct values", () => {
    cy.get('a[id="btn-register"]').click();

    const randomUsername = uuidv4();

    cy.get('form[id="form-register-user"]')
      .find('input[name="username"]')
      .focus()
      .type("admin" + randomUsername);

    cy.get('form[id="form-register-user"]')
      .find('input[name="email"]')
      .focus()
      .type("adminmail@gmail.com");

    cy.get('form[id="form-register-user"]')
      .find('input[name="password"]')
      .focus()
      .type("AdminPass123!");

    cy.get('form[id="form-register-user"]')
      .find('input[name="password1"]')
      .focus()
      .type("AdminPass123!");

    cy.get('form[id="form-register-user"]')
      .find('input[name="phone_number"]')
      .focus()
      .type("22225555");

    cy.get('form[id="form-register-user"]')
      .find('input[name="country"]')
      .focus()
      .type("Norway");

    cy.get('form[id="form-register-user"]')
      .find('input[name="city"]')
      .focus()
      .type("Oslo");

    cy.get('form[id="form-register-user"]')
      .find('input[name="street_address"]')
      .focus()
      .type("Slottsplassen 1");

    cy.get('input[id="btn-create-account"]').focus().click();

    cy.location("pathname", { timeout: 5000 }).should("include", "/workouts");
  });

  it("Boundary test incorrect email", () => {
    cy.get('a[id="btn-register"]').click();

    const randomUsername = uuidv4();

    cy.get('form[id="form-register-user"]')
      .find('input[name="username"]')
      .focus()
      .type("admin" + randomUsername);

    cy.get('form[id="form-register-user"]')
      .find('input[name="email"]')
      .focus()
      .type("thisisnoemail");

    cy.get('form[id="form-register-user"]')
      .find('input[name="password"]')
      .focus()
      .type("AdminPass123!");

    cy.get('form[id="form-register-user"]')
      .find('input[name="password1"]')
      .focus()
      .type("AdminPass123!");

    cy.get('form[id="form-register-user"]')
      .find('input[name="phone_number"]')
      .focus()
      .type("22225555");

    cy.get('form[id="form-register-user"]')
      .find('input[name="country"]')
      .focus()
      .type("Norway");

    cy.get('form[id="form-register-user"]')
      .find('input[name="city"]')
      .focus()
      .type("Oslo");

    cy.get('form[id="form-register-user"]')
      .find('input[name="street_address"]')
      .focus()
      .type("Slottsplassen 1");

    cy.get('input[id="btn-create-account"]').focus().click();

    cy.wait(3000);
    cy.location("pathname").should("include", "/register");
  });

  it("Boundary test 49 digit phone number", () => {
    cy.get('a[id="btn-register"]').click();

    const randomUsername = uuidv4();

    cy.get('form[id="form-register-user"]')
      .find('input[name="username"]')
      .focus()
      .type("admin" + randomUsername);

    cy.get('form[id="form-register-user"]')
      .find('input[name="email"]')
      .focus()
      .type("admin@gmail.com");

    cy.get('form[id="form-register-user"]')
      .find('input[name="password"]')
      .focus()
      .type("AdminPass123!");

    cy.get('form[id="form-register-user"]')
      .find('input[name="password1"]')
      .focus()
      .type("AdminPass123!");

    // 50 characters
    const randomPhoneNumber = Array.from({ length: 49 }, () =>
      Math.floor(Math.random() * 10).toString()
    ).join("");

    cy.get('form[id="form-register-user"]')
      .find('input[name="phone_number"]')
      .focus()
      .type(randomPhoneNumber);

    cy.get('form[id="form-register-user"]')
      .find('input[name="country"]')
      .focus()
      .type("Norway");

    cy.get('form[id="form-register-user"]')
      .find('input[name="city"]')
      .focus()
      .type("Oslo");

    cy.get('form[id="form-register-user"]')
      .find('input[name="street_address"]')
      .focus()
      .type("Slottsplassen 1");

    cy.get('input[id="btn-create-account"]').focus().click();

    cy.location("pathname", { timeout: 5000 }).should("include", "/workouts");
  });

  it("Boundary test exactly 50 digit phone number", () => {
    cy.get('a[id="btn-register"]').click();

    const randomUsername = uuidv4();

    cy.get('form[id="form-register-user"]')
      .find('input[name="username"]')
      .focus()
      .type("admin" + randomUsername);

    cy.get('form[id="form-register-user"]')
      .find('input[name="email"]')
      .focus()
      .type("admin@gmail.com");

    cy.get('form[id="form-register-user"]')
      .find('input[name="password"]')
      .focus()
      .type("AdminPass123!");

    cy.get('form[id="form-register-user"]')
      .find('input[name="password1"]')
      .focus()
      .type("AdminPass123!");

    // 50 characters
    const randomPhoneNumber = Array.from({ length: 50 }, () =>
      Math.floor(Math.random() * 10).toString()
    ).join("");

    cy.get('form[id="form-register-user"]')
      .find('input[name="phone_number"]')
      .focus()
      .type(randomPhoneNumber);

    cy.get('form[id="form-register-user"]')
      .find('input[name="country"]')
      .focus()
      .type("Norway");

    cy.get('form[id="form-register-user"]')
      .find('input[name="city"]')
      .focus()
      .type("Oslo");

    cy.get('form[id="form-register-user"]')
      .find('input[name="street_address"]')
      .focus()
      .type("Slottsplassen 1");

    cy.get('input[id="btn-create-account"]').focus().click();

    cy.location("pathname", { timeout: 5000 }).should("include", "/workouts");
  });

  it("Boundary test 51 digit phone number", () => {
    cy.get('a[id="btn-register"]').click();

    const randomUsername = uuidv4();

    cy.get('form[id="form-register-user"]')
      .find('input[name="username"]')
      .focus()
      .type("admin" + randomUsername);

    cy.get('form[id="form-register-user"]')
      .find('input[name="email"]')
      .focus()
      .type("admin@gmail.com");

    cy.get('form[id="form-register-user"]')
      .find('input[name="password"]')
      .focus()
      .type("AdminPass123!");

    cy.get('form[id="form-register-user"]')
      .find('input[name="password1"]')
      .focus()
      .type("AdminPass123!");

    // 50 characters
    const randomPhoneNumber = Array.from({ length: 51 }, () =>
      Math.floor(Math.random() * 10).toString()
    ).join("");

    cy.get('form[id="form-register-user"]')
      .find('input[name="phone_number"]')
      .focus()
      .type(randomPhoneNumber);

    cy.get('form[id="form-register-user"]')
      .find('input[name="country"]')
      .focus()
      .type("Norway");

    cy.get('form[id="form-register-user"]')
      .find('input[name="city"]')
      .focus()
      .type("Oslo");

    cy.get('form[id="form-register-user"]')
      .find('input[name="street_address"]')
      .focus()
      .type("Slottsplassen 1");

    cy.get('input[id="btn-create-account"]').focus().click();

    cy.wait(3000);
    cy.location("pathname").should("include", "/register");
  });
});
