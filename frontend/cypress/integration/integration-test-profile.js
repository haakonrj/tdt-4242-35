import { v4 as uuidv4 } from "uuid";

function getRandomDigits(n){
    return Array.from({ length: n }, () =>
        Math.floor(Math.random() * 10).toString()
      ).join("");
  }

Cypress.Commands.add('login', (username, password) => {
    cy.visit("http://localhost:8000/login.html");
    cy.get('form[id="form-login"]').find('input[name="username"]').focus().type(username)
    cy.get('form[id="form-login"]').find('input[name="password"]').focus().type(password)
    cy.get('form[id="form-login"]').find('input[id="btn-login"]').focus().click()
})

Cypress.Commands.add('editUserDetails', () => {
    cy.get('form[id="form-profile"]').find('input[id="btn-edit-profile"]').focus().click()
})

Cypress.Commands.add('checkActiveFields', () => {
    cy.get('form[id="form-profile"]').each($input => {
        cy.wrap($input).find('input[class="form-control"]').should('not.be.disabled')
    })
})

Cypress.Commands.add('saveAndReload', () => {
    cy.get('form[id="form-profile"]').find('input[id="btn-save-profile"]').focus().click()
    cy.wait(4000)
    cy.reload()
})


describe("Integration test of new profile page page", () => {
  beforeEach(() => {
    cy.login('admin', 'admin')
  });

  afterEach(() => {
      cy.visit('http://localhost:8000/logout.html')
  });

  it("Test and update profile page", () => {
    cy.get('a[id="nav-profile"]').should('be.visible').focus().click()
    
    //Check that all fields are disabled before edit button is pressed and active after
    cy.get('form[id="form-profile"]').each($input => {
        cy.wrap($input).find('input[class="form-control"]').should('be.disabled')
    })
    cy.editUserDetails()
    cy.checkActiveFields()
    
    // Update phone number
    const newPhoneNum = getRandomDigits(8)
    console.log(newPhoneNum)
    cy.get('form[id="form-profile"]').find('input[id="input-profile-phone"]').focus().clear().type(newPhoneNum)
    
    // Update country
    const newCountry = "Norway" + uuidv4()
    cy.get('form[id="form-profile"]').find('input[id="input-profile-country"]').focus().clear().type(newCountry)

    //Update City
    const newCity = "Norway" + uuidv4()
    cy.get('form[id="form-profile"]').find('input[id="input-profile-city"]').focus().clear().type(newCity)

    // Update street addess 
    const newStreet = "Street" + uuidv4()
    cy.get('form[id="form-profile"]').find('input[id="input-profile-street"]').focus().clear().type(newStreet)

    // Update checkbox
    cy.get('input[id="checkbox-profile-coach"]').then(($checkbox) =>{
        const isChecked = $checkbox.is(":checked")
        cy.log(isChecked)
        if (isChecked){
            cy.wrap($checkbox).uncheck()
        } else {
            cy.wrap($checkbox).check()
        }
        // Verify changes
        cy.saveAndReload()
        cy.get('input[id="input-profile-phone"]').should('have.value', newPhoneNum)
        cy.get('input[id="input-profile-country"]').should('have.value', newCountry)
        cy.get('input[id="input-profile-city"]').should('have.value', newCity)
        cy.get('input[id="input-profile-street"]').should('have.value', newStreet)
        cy.editUserDetails()
        cy.log(isChecked)
        if (isChecked){
            cy.get('input[id="checkbox-profile-coach"]').should('not.be.checked')
            cy.get('input[id="checkbox-profile-coach"]').check()

        } else {
            cy.get('input[id="checkbox-profile-coach"]').should('be.checked')
            cy.get('input[id="checkbox-profile-coach"]').uncheck()
        }
        cy.saveAndReload()
        cy.editUserDetails()
        if (isChecked){
            cy.get('input[id="checkbox-profile-coach"]').should('be.checked')

        } else {
            cy.get('input[id="checkbox-profile-coach"]').should('not.be.checked')
        }
    })
  });

  it('Test, select, and choose coach', () => {
    cy.get('a[id="nav-mycoach"]').should('be.visible').focus().click()
    cy.wait(4000)
    cy.get('div[id="coach-profile-container"]').then(($ele) => {
        // Check if the user dose not have a coach
        if ($ele.hasClass('hide')){
            cy.get('ul[id="mycoach-coach-list"]').children().each(($coach) => {
                cy.wrap($coach).should('not.have.class', 'selected')
            })
            //Select new coach
            cy.get('ul[id="mycoach-coach-list"]').children().first().click()
            cy.get('input[id="button-select-coach"]').focus().click()
            cy.wait(1000)
            cy.get('div[id="coach-profile-container"]').should('not.have.class', 'hide')
        } 
        // Check if the user has a coach
        else {
            cy.get('ul[id="mycoach-coach-list"]').children().each(($coach) => {
                $coach.hasClass('selected')
                // Checks that the coach input field is equal to the coach selected from the list
                if ($coach.hasClass('selected')) {
                    const coachName = $coach.text()
                    cy.get('input[id="input-coach"]').should('have.value', coachName)
                }
            })
        }
    })
  });
});
