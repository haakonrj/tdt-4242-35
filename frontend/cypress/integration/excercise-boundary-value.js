import { v4 as uuidv4 } from "uuid";


Cypress.Commands.add('login', (username, password) => {
    cy.visit("http://localhost:8000/login.html");
    cy.get('form[id="form-login"]').find('input[name="username"]').focus().type(username)
    cy.get('form[id="form-login"]').find('input[name="password"]').focus().type(password)
    cy.get('form[id="form-login"]').find('input[id="btn-login"]').focus().click()
})

describe("Test excercise page", () => {
  beforeEach(() => {
    cy.login('admin', 'admin')
    cy.visit("http://localhost:8000/exercise.html");
    // Fill name
    cy.get('form[id="form-exercise"]').find('input[name="name"]').focus().type("excercise" + uuidv4())
    // Fill description
    cy.get('form[id="form-exercise"]').find('textarea[id="inputDescription"]').focus().type('d')
  });

  afterEach(() => {
      cy.visit('http://localhost:8000/logout.html')
  });

  it("Test all correct fields excercise", () => {
    cy.get('form[id="form-exercise"]').find('input[id="inputUnit"]').focus().type('1')
    cy.get('form[id="form-exercise"]').find('input[id="inputDuration"]').focus().type('1')
    cy.get('form[id="form-exercise"]').find('input[id="inputCalories"]').focus().type('1')
    cy.get('form[id="form-exercise"]').find('input[id="btn-ok-exercise"]').focus().click()
    cy.get('input[id="btn-create-exercise"]').should('be.visible');
  });
  it("Boundary test units 0", () => {
    cy.get('form[id="form-exercise"]').find('input[id="inputUnit"]').focus().type('0')
    cy.get('form[id="form-exercise"]').find('input[id="inputDuration"]').focus().type('1')
    cy.get('form[id="form-exercise"]').find('input[id="inputCalories"]').focus().type('1')
    cy.get('form[id="form-exercise"]').find('input[id="btn-ok-exercise"]').focus().click()
    cy.get('div[class="alert alert-warning alert-dismissible fade show"]').should(
        "have.text",
        "units"
    );
  });
  it("Boundary test units -1", () => {
    cy.get('form[id="form-exercise"]').find('input[id="inputUnit"]').focus().type('-0')
    cy.get('form[id="form-exercise"]').find('input[id="inputDuration"]').focus().type('1')
    cy.get('form[id="form-exercise"]').find('input[id="inputCalories"]').focus().type('1')
    cy.get('form[id="form-exercise"]').find('input[id="btn-ok-exercise"]').focus().click()
    cy.get('div[class="alert alert-warning alert-dismissible fade show"]').should(
        "have.text",
        "units"
    );
  });
  it("Boundary test duration -1", () => {
    cy.get('form[id="form-exercise"]').find('input[id="inputUnit"]').focus().type('1')
    cy.get('form[id="form-exercise"]').find('input[id="inputDuration"]').focus().type('-1')
    cy.get('form[id="form-exercise"]').find('input[id="inputCalories"]').focus().type('1')
    cy.get('form[id="form-exercise"]').find('input[id="btn-ok-exercise"]').focus().click()
    cy.get('div[class="alert alert-warning alert-dismissible fade show"]').should(
        "have.text",
        "duration"
    );
  });
  it("Boundary test duration 0", () => {
    cy.get('form[id="form-exercise"]').find('input[id="inputUnit"]').focus().type('1')
    cy.get('form[id="form-exercise"]').find('input[id="inputDuration"]').focus().type('0')
    cy.get('form[id="form-exercise"]').find('input[id="inputCalories"]').focus().type('1')
    cy.get('form[id="form-exercise"]').find('input[id="btn-ok-exercise"]').focus().click()
    cy.get('div[class="alert alert-warning alert-dismissible fade show"]').should(
        "have.text",
        "duration"
    );
  });
  it("Boundary test calories 0", () => {
    cy.get('form[id="form-exercise"]').find('input[id="inputUnit"]').focus().type('1')
    cy.get('form[id="form-exercise"]').find('input[id="inputDuration"]').focus().type('1')
    cy.get('form[id="form-exercise"]').find('input[id="inputCalories"]').focus().type('0')
    cy.get('form[id="form-exercise"]').find('input[id="btn-ok-exercise"]').focus().click()
    cy.get('div[class="alert alert-warning alert-dismissible fade show"]').should(
        "have.text",
        "calories"
    );
  });
  it("Boundary test calories -1", () => {
    cy.get('form[id="form-exercise"]').find('input[id="inputUnit"]').focus().type('1')
    cy.get('form[id="form-exercise"]').find('input[id="inputDuration"]').focus().type('1')
    cy.get('form[id="form-exercise"]').find('input[id="inputCalories"]').focus().type('-1')
    cy.get('form[id="form-exercise"]').find('input[id="btn-ok-exercise"]').focus().click()
    cy.get('div[class="alert alert-warning alert-dismissible fade show"]').should(
        "have.text",
        "calories"
    );
  });
});
