import { v4 as uuidv4 } from "uuid";

const athleteName = "athlete" + uuidv4();
const coachName = "coach" + uuidv4();

Cypress.Commands.add(
  "register",
  (
    username,
    email,
    password,
    password1,
    phone,
    country,
    city,
    street,
    isCoach
  ) => {
    cy.visit("http://localhost:8000/register.html");
    cy.get('form[id="form-register-user"]')
      .find('input[name="username"]')
      .type(username);
    cy.get('form[id="form-register-user"]')
      .find('input[name="email"]')
      .type(email);
    cy.get('form[id="form-register-user"]')
      .find('input[name="password"]')
      .type(password);
    cy.get('form[id="form-register-user"]')
      .find('input[name="password1"]')
      .type(password1);
    cy.get('form[id="form-register-user"]')
      .find("[name=phone_number]")
      .type(phone);
    cy.get('form[id="form-register-user"]')
      .find("[name=country]")
      .type(country);
    cy.get('form[id="form-register-user"]').find("[name=city]").type(city);
    cy.get('form[id="form-register-user"]')
      .find("[name=street_address]")
      .type(street);
    if (isCoach) {
      cy.get('form[id="form-register-user"]').find("[name=is_coach]").check();
    } else {
      cy.get('form[id="form-register-user"]').find("[name=is_coach]").uncheck();
    }
    cy.get('form[id="form-register-user"]').find("#btn-create-account").click();
  }
);

Cypress.Commands.add("login", (username, password) => {
  cy.visit("http://localhost:8000/login.html");
  cy.get('form[id="form-login"]')
    .find('input[name="username"]')
    .focus()
    .type(username);
  cy.get('form[id="form-login"]')
    .find('input[name="password"]')
    .focus()
    .type(password);
  cy.get('form[id="form-login"]').find('input[id="btn-login"]').focus().click();
});

Cypress.Commands.add("validateWorkout", () => {
  cy.url().should("include", "/workouts.html");
  cy.contains("New workout").should("be.visible");
  cy.contains("13:00").should("be.visible");
  cy.contains(athleteName).should("be.visible");
});

describe("Black-box testing of FR5 - workouts", () => {
  before(() => {
    // register new athlete
    cy.register(
      athleteName,
      "athlete@email.com",
      "Password123!",
      "Password123!",
      "22225555",
      "Norge",
      "Trondheim",
      "Fjordgata 1",
      false
    );
    cy.wait(500);
    cy.get("#btn-logout").click();
    cy.wait(500);

    // register new coach
    cy.register(
      coachName,
      "coach@email.com",
      "Password123!",
      "Password123!",
      "22225555",
      "Norge",
      "Trondheim",
      "Fjordgata 1",
      true
    );
    cy.wait(500);
    cy.get("#btn-logout").click();
    cy.wait(500);
  });

  it("should connect coach to athlete", () => {
    cy.login(coachName, "Password123!");
    cy.wait(500);

    cy.visit("http://localhost:8000/myathletes.html");
    cy.get('input[name="athlete"]').type(athleteName);
    cy.get("#button-submit-roster").click();
    cy.wait(500);
    cy.get("#btn-logout", { timeout: 500 }).click();
    cy.wait(500);

    cy.login(athleteName, "Password123!");
    cy.wait(500);
    cy.visit("http://localhost:8000/mycoach.html");
    cy.get("button[type=button]")
      .should("have.class", "btn btn-success")
      .contains("Accept")
      .click();
    cy.wait(500);
    cy.get("#input-coach").should("have.value", coachName);
    cy.wait(500);
    cy.get("#btn-logout", { timeout: 500 }).click();
    cy.wait(500);
  });

  it("should create public workout", () => {
    cy.login(athleteName, "Password123!");
    cy.wait(500);
    cy.visit("http://localhost:8000/workouts.html");

    cy.get("#btn-create-workout").click();
    cy.wait(500);

    cy.get("#inputName").type("New workout");
    cy.get("#inputDateTime").type("2022-01-01T13:00");
    cy.get("#inputVisibility").select("Public");
    cy.get("#inputNotes").type("This is a public workout");

    cy.get("#btn-ok-workout").click();
    cy.wait(500);

    cy.validateWorkout();

    cy.wait(500);
    cy.get("#btn-logout", { timeout: 500 }).click();
    cy.wait(500);
  });

  it('should display workout in "Public Workouts" as athlete', () => {
    cy.login(athleteName, "Password123!");
    cy.wait(500);
    cy.visit("http://localhost:8000/workouts.html");

    cy.contains("Public Workouts").click();
    cy.wait(500);

    cy.validateWorkout();

    cy.wait(500);
    cy.get("#btn-logout", { timeout: 500 }).click();
    cy.wait(500);
  });

  it('should display workout in "Athlete Workouts" as athlete', () => {
    cy.login(athleteName, "Password123!");
    cy.wait(500);
    cy.visit("http://localhost:8000/workouts.html");

    cy.contains("Athlete Workouts").click();
    cy.wait(500);

    cy.validateWorkout();

    cy.wait(500);
    cy.get("#btn-logout", { timeout: 500 }).click();
    cy.wait(500);
  });

  it('should display workout in "Public Workouts" as coach', () => {
    cy.login(coachName, "Password123!");
    cy.wait(500);
    cy.visit("http://localhost:8000/workouts.html");

    cy.contains("Public Workouts").click();
    cy.wait(500);

    cy.validateWorkout();

    cy.wait(500);
    cy.get("#btn-logout", { timeout: 500 }).click();
    cy.wait(500);
  });

  it('should display workout in "Athlete Workouts" as coach', () => {
    cy.login(coachName, "Password123!");
    cy.wait(500);
    cy.visit("http://localhost:8000/workouts.html");

    cy.contains("Athlete Workouts").click();
    cy.wait(500);

    cy.validateWorkout();

    cy.wait(500);
    cy.get("#btn-logout", { timeout: 500 }).click();
  });
});
