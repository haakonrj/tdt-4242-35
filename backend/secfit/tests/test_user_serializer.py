from operator import is_
from django.test import TestCase
from users.serializers import UserSerializer

class UserSerializerTestCase(TestCase):
  def setUp(self):
    self.serializer = UserSerializer()
  
  def test_validate_password(self):
    try:
      password = "Password123!!"
      response = self.serializer.validate_password(password)
      self.assertEqual(response, password)
    except:
      self.fail("validate password raised error")

  def test_create(self):
    username = "testUser"
    email = "testUser@test.com"
    password = "Password123!!"
    phone_number = 99995555
    country = "Norway"
    city = "Trondheim"
    street_address = "Fjordgata 1"
    is_coach = False

    form = {
      "username": username, 
      "email": email, 
      "password": password, 
      "phone_number": phone_number, 
      "country": country, 
      "city": city, 
      "street_address": street_address, 
      "is_coach": is_coach
    }

    user_obj = self.serializer.create(form)

    self.assertEqual(username, user_obj.username)
    self.assertEqual(email, user_obj.email)
    self.assertEqual(phone_number, user_obj.phone_number)
    self.assertEqual(country, user_obj.country)
    self.assertEqual(city, user_obj.city)
    self.assertEqual(street_address, user_obj.street_address)
    self.assertEqual(is_coach, user_obj.is_coach)