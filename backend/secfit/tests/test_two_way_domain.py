import itertools
from rest_framework.test import APITestCase

class TwoWayDomainTest(APITestCase):
  def test_2_way(self):
    # First field is valid, second is above limit, third is empty and fourth is something else invalid
    # Thus, only first field is actually valid
    # A field is a tuple of the value and its validity
    data = {
      "username": [("someUser", True), ("a"*51, False), ("", False), ("9Aj[åÆ!*#", False)],
      "email": [("user@user.user", True), ("a"*51+"@email.no", False), ("", False), ("invalidemail", False)],
      "password": [("Password123!", True), ("r"*51, False), ("", False), ("a", False)],
      "password1": [("Password123!", True), ("a"*51, False), ("", False), ("a", False)],
      "phone_number": [("47431234", True), ("1"*51, False), ("-1", False)],
      "country": [("SomeCountry", True), ("a"*51, False), ("-1", False)],
      "city": [("SomeCity", True), ("a"*51, False), ("", False)],
      "street_address": [("SomeStreet", True), ("a"*51, False), ("", False)],
      "is_coach": [(True, True), (False, True)]
    }
    
    # All combinations of values as a list
    combinations = itertools.product(*data.values())

    # Combinations mapped to test format
    tests = [(dict(zip(data.keys(), entry))) for entry in combinations]

    # Make usernames unique so tests don't fail because username already exists in database
    for i, test in enumerate(tests):
      new_username = (tests[i]["username"][0] + str(i), tests[i]["username"][1])
      tests[i]["username"] = new_username

    # Run tests
    for test in tests:
      field_validities = [value[1] for value in test.values()]
      expectedResponse = 201 if all(field_validities) else 400
      formatted_test = {key: value[0] for key, value in test.items()}
      response = self.client.post('/api/users/', formatted_test)
      self.assertEqual(response.status_code, expectedResponse)