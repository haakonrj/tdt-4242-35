from django.test import TestCase, RequestFactory
from users.models import User
from workouts.models import Exercise, ExerciseInstance
from workouts.permissions import *

class WorkoutPermissionsTest(TestCase):

    def setUp(self):
        self.coach_user = User.objects.create(username="coachUser", email="coach_user@email.com", phone_number="11111111",
                                         country="Norway", city="Oslo", street_address="Gate 3", is_coach=True)
        self.user1 = User.objects.create(username="user1", email="user1@email.com", phone_number="99999999",
                                         country="Norway", city="Trondheim", street_address="Gate 1", is_coach=False, coach=self.coach_user)
        self.user2 = User.objects.create(username="user2", email="user2@email.com", phone_number="88888888",
                                         country="NotNorway", city="Oslo", street_address="Gate 2", is_coach=False)
        self.workout = Workout.objects.create(name="Some workout", date="2022-01-10T04:10:00Z", notes="Some note",
                                         owner=self.user1, visibility="PU")
        self.workout_private = Workout.objects.create(name="Some workout", date="2022-01-10T04:10:00Z", notes="Some note",
                                              owner=self.user1, visibility="PR")
        self.request_factory = RequestFactory()

    def test_is_owner(self):
        self.is_owner = IsOwner()
        get_request = self.request_factory.get('/')
        get_request.user = self.user1
        self.assertTrue(self.is_owner.has_object_permission(get_request, None, self.workout))
        get_request.user = self.user2
        self.assertFalse(self.is_owner.has_object_permission(get_request, None, self.workout))

    def test_is_owner_of_workout(self):
        self.is_workout_owner = IsOwnerOfWorkout()
        get_request = self.request_factory.get('/')
        get_request.user = self.user1

        post_request_no_data = self.request_factory.post('/')
        post_request_no_data.data = {}

        post_request = self.request_factory.post('/')
        post_request.data = {"workout": "/api/workouts/1/"}
        post_request.user = self.user1

        #Check has_object_permission (no idea why they use obj.workout when we know there should be a Workout object...)
        exercise = Exercise.objects.create(name="stuff", description="some description", unit="Times")
        exercise_instance = ExerciseInstance.objects.create(workout=self.workout, exercise=exercise, sets=1, number=6)
        self.assertTrue(self.is_workout_owner.has_object_permission(get_request, None, exercise_instance))

        # Should fail when checking if request == POST and return True
        self.assertTrue(self.is_workout_owner.has_permission(get_request, None))

        # Should fail when getting data from the post request and return False
        self.assertFalse(self.is_workout_owner.has_permission(post_request_no_data, None))

        # Should return True when the user of the request is owner of the workout
        self.assertTrue(self.is_workout_owner.has_permission(post_request, None))

        # Should return False when the user of the request is not equal to the user of owner of the workout
        post_request.user = self.user2
        self.assertFalse(self.is_workout_owner.has_permission(post_request, None))



    def test_is_coach_and_visible_to_coach(self):
        is_coach_and_visible = IsCoachAndVisibleToCoach()
        get_request = self.request_factory.get('/')
        get_request.user = self.coach_user

        self.assertTrue(is_coach_and_visible.has_object_permission(get_request, None, self.workout))
        self.user1.coach = None
        self.assertFalse(is_coach_and_visible.has_object_permission(get_request, None, self.workout))
        # Not public should return False
        self.assertFalse(is_coach_and_visible.has_object_permission(get_request, None, self.workout_private))



    def test_is_coach_of_workout_and_visible_to_coach(self):
        is_coach_of_workout_visible = IsCoachOfWorkoutAndVisibleToCoach()
        get_request = self.request_factory.get('/')
        get_request.user = self.coach_user

        exercise = Exercise.objects.create(name="stuff", description="some description", unit="Times")
        exercise_instance = ExerciseInstance.objects.create(workout=self.workout, exercise=exercise, sets=1, number=6)

        # Check correct coach user and public & coach visibility
        self.assertTrue(is_coach_of_workout_visible.has_object_permission(get_request, None, exercise_instance))
        exercise_instance.workout.visibility = 'CO'
        self.assertTrue(is_coach_of_workout_visible.has_object_permission(get_request, None, exercise_instance))

        # Test correct coach user, but private visibility.
        exercise_instance.workout.visibility = 'PR'
        # The below code should be false, probably a security vulnerability.
        #self.assertFalse(is_coach_of_workout_visible.has_object_permission(get_request, None, exercise_instance))

        # Test wrong user
        get_request.user = self.user2
        exercise_instance.workout.visibility = 'PU'
        self.assertFalse(is_coach_of_workout_visible.has_object_permission(get_request, None, exercise_instance))


    def test_is_public(self):
        is_public = IsPublic()
        get_request = self.request_factory.get('/')

        self.assertTrue(is_public.has_object_permission(get_request, None, self.workout))
        self.assertFalse(is_public.has_object_permission(get_request, None, self.workout_private))

    def test_is_workout_public(self):
        is_workout_public = IsWorkoutPublic()
        get_request = self.request_factory.get('/')

        exercise = Exercise.objects.create(name="stuff", description="some description", unit="Times")
        exercise_instance = ExerciseInstance.objects.create(workout=self.workout, exercise=exercise, sets=1, number=6)

        self.assertTrue(is_workout_public.has_object_permission(get_request, None, exercise_instance))
        exercise_instance.workout = self.workout_private
        self.assertFalse(is_workout_public.has_object_permission(get_request, None, exercise_instance))

    def test_is_read_only(self):
        is_read_only = IsReadOnly()
        get_request = self.request_factory.get('/')
        head_request = self.request_factory.head('/')
        options_request = self.request_factory.options('/')
        post_request = self.request_factory.post('/')

        self.assertTrue(is_read_only.has_object_permission(get_request, None, None))
        self.assertTrue(is_read_only.has_object_permission(head_request, None, None))
        self.assertTrue(is_read_only.has_object_permission(options_request, None, None))
        self.assertFalse(is_read_only.has_object_permission(post_request, None, None))
